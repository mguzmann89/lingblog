blogdown::build_site(build_rmd = TRUE)
blogdown::build_site(build_rmd = "newfile")
blogdown::build_site(build_rmd = "timestamp")
blogdown::clean_duplicates(preview = FALSE)
blogdown::check_site()
blogdown::new_post("Gaussian Process for spatial modeling", kind = "rmd")

system("conda list | grep -E -i -w \"^$(conda env export --no-builds --from-history | awk '$1 == \"-\"{ if (key == \"dependencies:\") print $NF; next } {key=$1}' | sed 's/=.*//' | tr -s '\r\n' '|' | sed 's/|$//')\\s\" | awk '{ print $1 \"=\" $2 }' | sed 's/^/  - /'")
