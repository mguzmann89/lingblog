functions {
  matrix cov_matern52(matrix D, real lscale, real sdgp) {
    int N = dims(D)[1];
    int M = dims(D)[2];
    matrix[N, M] K;
    for (i in 1:N) {
      for (j in 1:M) {
        real x52 = (sqrt(5) * D[i,j])/lscale;
        K[i, j] = sdgp^2 * (1 + x52 + (x52^2)/3)* exp(-x52);
      }
    }
    return K;
  }
  vector gp_pred_rng(int N1,
		     int N2,
		     int K,
		     matrix old_dist,
		     matrix new_dist_cross,
                     matrix new_dist_full,
		     vector yL,
                     real lscale,
                     real sdgp) {
    vector[N2] f2;
    {
      matrix[N1, N1] L_K;
      vector[N1] K_div_y1;
      matrix[K, N2] k_x1_x2;
      matrix[N1, N2] v_pred;
      vector[N2] f2_mu;
      matrix[N2, N2] cov_f2;
      matrix[N2, N2] diag_delta;
      L_K = cholesky_decompose(add_diag(cov_matern52(old_dist, lscale, sdgp), 1e-12));
      K_div_y1 = mdivide_left_tri_low(L_K, yL);
      K_div_y1 = mdivide_right_tri_low(K_div_y1', L_K)';
      k_x1_x2 = cov_matern52(new_dist_cross, lscale, sdgp)';
      f2_mu = (k_x1_x2' * K_div_y1);
      v_pred = mdivide_left_tri_low(L_K, k_x1_x2);
      cov_f2 = cov_matern52(new_dist_full, lscale, sdgp) - v_pred' * v_pred;
      diag_delta = diag_matrix(rep_vector(1e-12, N2));
      f2 = multi_normal_rng(f2_mu, cov_f2 + diag_delta);
    }
    return f2;
  }
}
data {
  int N1;
  int N2;
  int K;
  real lscale;
  real sdgp;
  vector[N1] yL;
  matrix[N1,N1] old_dist;
  matrix[N2,N2] new_dist_full;
  matrix[N2,K] new_dist_cross;
}
generated quantities {
  vector[N2] preds = gp_pred_rng(N1, N2, K, old_dist, new_dist_cross, new_dist_full, yL, lscale, sdgp);
}
